function Mostrar(a)
{
    var e=document.getElementById(a);
    if(!e)return true;

    if(e.style.display=="none")
    {
        e.style.display="block"
    }
    else{
        e.style.display="none"
    }
    return true;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if(charCode == 46)
        return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57 ))
        return false;

    return true;

}