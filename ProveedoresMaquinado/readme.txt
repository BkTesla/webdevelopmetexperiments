Este proyecto es una simulacion de una empresa ensambladora que tiene proveedores de maquinado especificos los cuales por medio de esta aplicacion web reciben los requerimentos de las piezas y planos de las mismas y pueden realizar una cotizacion de la misma.

Funcionalidades
-Perfiles de usuario (maquinado y usuario interno).
-vista previa de planos;
-ver planos aprobados por la empresa para maquinado.
-posibilidad de negociacion para el precio final de la pieza.
