<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="../css/cotizar.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Detalles Cotizaciones</title>
    </head>
    <body class="text-center">

        <div class="container text-center">
			<div class="row">
				<div class="col-sm-12">
                    <?php $image = array(
                        'src' => 'img/logo.png',
                        'class' => 'img-fluid',
                        'id' => 'logo'
                    );?>
                    <?php echo img($image);?>
				</div>
			</div>
        </div>
        <div class="container-fluid">
            <hr id="line">
            <h1><?= $Datos['estatus']?></h1>
            <hr id="line">
        </div>


        <div class="modal-body row">
            <div class="col-md-6">
                <?php
                    if(($cargarImagen != NULL)){ 
                ?>
                        <img src="../img/imagen.jpg" alt="imagen" class="plano">
                <?php 
                    }else{
                ?>
                    <img src="../img/file_not_found.png" alt="imagen" class="plano">
                <?php } ?>
            </div>
            <div class="col-md-6">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Precio inicial</th>
                            <th>Tiempo inical</th>
                            <th>Precio negociacion</th>
                            <th>Tiempo negociacion</th>
                            <th>Precion Final</th>
                            <th>Tiempo Final</th>
                        </tr>
                    </thead>
                
                <tr class="table-active">    
                    <td><? echo '$'.$Datos['precioInicial'] ?></td>
                    <td><? echo $Datos['tiempoInicial'].' Dias' ?></td>
                    <td><?echo '$'.$Datos['precioNegociacion']?></td>
                    <td><?echo $Datos['tiempoNegociacion'].' Dias'?></td>
                    <td><? echo '$'.$Datos['precioFinal']?></td>
                    <td><? echo $Datos['tiempoFinal'].' Dias'?></td>
                </tr>
                </table>
            </div>
        </div>
        
        <form action="<?=site_url('HomeController/index'); ?>">
            <input type="submit" value="Regresar" class="btn btn-secondary"> 
        </form>
    </body>
</html>