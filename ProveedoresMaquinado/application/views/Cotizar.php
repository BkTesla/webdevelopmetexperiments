<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="../css/cotizar.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <script src="../js/cotizar.js"></script> 
        <script src="../js/ocultarElemento.js"></script>
        <title>Cotizar</title>
    </head>
    <body class="text-center">

            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-12">
                        <?php $image = array(
                            'src' => 'img/logo.png',
                            'class' => 'img-fluid',
                            'id' => 'logo'
                        );?>
                        <?php echo img($image);?>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <hr id="line">
                <h1>Cotizar</h1>
                <hr id="line">
            </div>
            
            <div class="modal-body row">
                <div class="col-md-6">
                <?php
                if(($cargarImagen != NULL)){ ?>`
                    <img src="../img/imagen.jpg" alt="imagen" class="plano">
                <?php 
                }else{
                ?>
                    <img src="../img/file_not_found.png" alt="imagen" class="plano">
                <?php } ?>
                </div>
                <div class="col-md-6">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th>Opcion</th>
                                <th>Precio</th>
                                <th>Tiempo de Entrega</th>
                                <th>cotizar</th>
                            </tr>
                        </thead>
                        
                        <tr>
                        <?php if($opciones == '1'){?>
                            <td>Opcion 1(con material)</td>
                            <?php  echo form_open('CotizarController/CotizarConMaterial');?>
                                    <td><input type="text" name="precio" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                    <td><input type="text" name="tiempo" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                    <td><button type="submit" name="submit" formmethod="post" onclick="pregunta(event)" class="btn btn-info">enviar</button></td>
                                </form>
                        </tr>
                        <?php }elseif($opciones == '2'){?>
                        <tr>
                            <td>Opcion 2(sin material)</td>
                            <?php  echo form_open('CotizarController/CotizarSinMaterial');?>
                                <td><input type="text" name="precio" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                <td><input type="text" name="tiempo" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                <td><button type="submit" name="submit" formmethod="post" onclick="pregunta(event)" class="btn btn-info">enviar</button></td>
                            </form>

                        </tr>
                        <?php }elseif($opciones == '3'){?>
                        <tr>
                            <td>Opcion 3(extra work)</td>
                            <?php  echo form_open('CotizarController/ExtraWork');?>
                                <td><input type="text" name="precio" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                <td><input type="text" name="tiempo" onkeypress="return isNumberKey(event)" required class="form-control"></td>
                                <td><button type="submit" name="submit" formmethod="post" onclick="pregunta(event)" class="btn btn-info">enviar</button></td>
                            </form>
                        </tr>
                        <?php }?>
                    </table>
                </div>
            </div>
        <form action="<?=site_url('HomeController/index'); ?>">
            <input type="submit" value="Regresar" class="btn btn-secondary"> 
        </form>

        <script src="../boostrap/js/jquery-1.11.3.min.js"></script>
        <script src="../boostrap/js/bootstrap.min.js"></script>
    </body>
</html> 