<?php 
	$datos = $this->session->userdata('correo');
	$this->load->helper('html');
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="../js/cotizar.js"></script> 
    <script src="../js/ocultarElemento.js"></script>
		<title>Login proveedores</title>
	</head>
	<body class="text-center">
		<div class="container text-center">
			<div class="row">
				<div class="col-sm-12">
                    <?php $image = array(
                        'src' => 'img/logo.png',
                        'class' => 'img-fluid',
                        'id' => 'logo'
                    );?>
                    <?php echo img($image);?>
				</div>
			</div>
        </div>
		<br>
		<hr style="width: 100%; color: black; height: 1px; background-color:black;">
		<h1>Sistema de cotizaciones maquinado</h1>
		<hr style="width: 100%; color: black; height: 1px; background-color:black;">
		<?php echo form_open('HomeController/cargarDatos');?>
			<select name="estatus">
				<option value="PENDIENTE" <?if(isset($estatus) && $estatus == 'PENDIENTE'): echo "selected='PENDIENTE'"; endif?>>Pendiente</option>
				<option value="COTIZADO" <?if(isset($estatus) && $estatus == 'COTIZADO'): echo "selected='COTIZADO'"; endif?>>Cotizado</option>
				<option value="NEGOCIADO" <?if(isset($estatus) && $estatus == 'NEGOCIADO'): echo "selected='NEGOCIADO'"; endif?>>Negociado</option>
				<option value="NEGOCIACION ACEPTADA" <?if(isset($estatus) && $estatus == 'NEGOCIACION ACEPTADA'): echo "selected='NEGOCIACION ACEPTADA'"; endif?>>Negociacion aceptada</option>
				<option value="NEGOCIACION RECHAZADA" <?if(isset($estatus) && $estatus == 'NEGOCIACION RECHAZADA'): echo "selected='NEGOCIACION RECHAZADA'"; endif?>>Negociacion rechazada</option>
				<option value="CONTRAOFERTA" <?if(isset($estatus) && $estatus == 'CONTRAOFERTA'): echo "selected='CONTRAOFERTA'"; endif?>>Contraoferta</option>
				<option value="SELECCIONADO" <?if(isset($estatus) && $estatus == 'SELECCIONADO'): echo "selected='SELECCIONADO'"; endif?>>Seleccionado</option>
				<option value="DESCARTADO" <?if(isset($estatus) && $estatus == 'DESCARTADO'): echo "selected='DESCARTADO'"; endif?>>Descartadas</option>
				<option value="ORDEN DE COMPRA" <?if(isset($estatus) && $estatus == 'ORDEN DE COMPRA'): echo "selected='ORDEN DE COMPRA'"; endif?>>Orden de compra</option>
			</select>
			<br>
			<br>
			<input type="submit" value="Consultar" name="consultar" class="btn btn-secondary"/><br>
			<br>
		</form>
		<table class="table">
				<tr>
					<th style="display:none">id_plano</th>
					<th >id</th>
					<th>Nombre del plano</th>
					<th>Proceso</th>
					<th>Informacion</th>
				</tr>
				<?php
					if(!empty($cotizaciones))
					{
						if($cotizaciones->num_rows() > 0)
						{
							foreach ($cotizaciones->result() as $row) {
								?>
								<tr>
									<td style="display:none"><?php echo $row->id_plano; ?></td>
									<td ><?php echo $row->id; ?></td>
									<td><?php echo $row->nombre_archivo; ?></td> 
									<td><?php echo $row->proceso; ?></td>
									<?php
										$estatus = $row->estatus;
										if($estatus == 'PENDIENTE'){
											$datosCotizacion = $row->id.','.$row->nombre_archivo.','.$row->id_plano.','.$row->opcion;
									?>
									<td> <?php echo form_open('CotizarController');?> 
										<button name="cotizar" type="submit" formmethod="post" value="<?= $datosCotizacion;?>"  class="btn btn-info">Cotizar</button>
										</form>
									</td>
									<?php		
										}elseif($estatus == "NEGOCIADO"){	
											$datosNegocioacion = $row->id.','.$row->nombre_archivo.','.$row->id_plano;
									?>
										<td> 
											<?php echo form_open('NegociarController');?>
											<button type="submit" name="negociar" formmethod="post" value="<?= $datosNegocioacion?>"  class="btn btn-info">ver negociacion</button>
											</form>
										</td>
									<?php	
										}else{
											$datosNegocioacion = $row->id.','.$row->nombre_archivo.','.$row->id_plano;
									?>
									<td>
										<?php echo form_open('DetallesController');?>
										<button type="submit" name="detalles" formmethod="post" value="<?= $datosNegocioacion?>"  class="btn btn-info">ver detalles</button>
										</form>
									</td>
									<?php } ?>
								</tr>
							<?php
							}
						}
						else {
						?>
							<tr>
								<td> No exite ninguna cotizacion con este estatus</td>
							</tr>
						<?php
						}
					}
				?>
		</table>
		<form action="<?=base_url()?>">
			<input type="submit" value="Cerrar sesion" class="btn btn-secondary">
		</form>
	</body>
</html>