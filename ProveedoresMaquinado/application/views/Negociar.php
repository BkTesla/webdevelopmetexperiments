<!DOCTYPE html>
<html lang="en">
<head>
    <div>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" type="text/css" href="../css/cotizar.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script src="../js/cotizar.js"></script>
    <script src="../js/ocultarElemento.js"></script>
    <title>Negociacion</title>
</head>
<body class="text-center">

    <div class="container text-center">
		<div class="row">
			<div class="col-sm-12">
                <?php $image = array(
                    'src' => 'img/logo.png',
                    'class' => 'img-fluid',
                    'id' => 'logo'
                );?>
                <?php echo img($image);?>
			</div>
        </div>
    </div>
    <div class="container-fluid">
        <hr id="line">
        <h1>NEGOCIAR</h1>
        <hr id="line">
    </div>

    <div class="modal-body row">
        <div class="col-md-6">
            <?php
                if(($cargarImagen != NULL)){ 
            ?>
                    <img src="../img/imagen.jpg" alt="imagen" class="plano">
            <?php 
                }else{
            ?>
                    <img src="../img/file_not_found.png" alt="imagen" class="plano">
            <?php } ?>
        </div>
        <div class="col-md-6">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>Precio inicial</th>
                        <th>Tiempo inical</th>
                        <th>Precion negociacion</th>
                        <th>Tiempo negociacion</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tr>
                    
                    <td><? echo '$'.$Datos['precioInicial'] ?></td>
                    <td><? echo $Datos['tiempoInicial'].' Dias' ?></td>
                    <td><? echo '$'.$Datos['precioNegociacion']?></td>
                    <td><? echo $Datos['tiempoNegociacion'].' Dias'?></td>
                    <?php $enviarDatos = $Datos['precioNegociacion'].','.$Datos['tiempoNegociacion'];?>
                    
                    <?php echo form_open('NegociarController/AceptarNegociacion');?>
                    <td><button type="submit" name="aceptar" formmethod="post" value="<?=$enviarDatos?>" onclick="aceptarNegociacion(event, '¿Estas seguro que desea aceptar la negociacion?')" class="btn btn-info">Aceptar</button></td>
                    </form>

                    <?php echo form_open('NegociarController/RechazarNegociacion');?>
                    <td><button type="submit" name="rechazar" formmethod="post" value="<?=$enviarDatos?>" onclick="aceptarNegociacion(event, '¿Estas seguro que desea rechazar la negociacion?')" class="btn btn-info">Rechazar</button></td>
                    </form>

                    <td><input type="button" name="contraoferta" value="Contraoferta"  onclick="return Mostrar('ocultar')" class="btn btn-info"></td>
                </tr>
            </table>
            <table id="ocultar" class="table text-rigth" style="display:none">
                <thead>
                    <tr>
                        <th>Precio Contraoferta</th>
                        <th>Tiempo Contraoferta</th>
                        <th></th>
                    </tr>
                </thead>
                <tr>
                    <?php echo form_open('NegociarController/Contraoferta');?>
                    <td><input type="text" name="precio" onkeypress="return isNumberKey(event)" required></td>
                    <td><input type="text" name="tiempo" onkeypress="return isNumberKey(event)" required></td>
                    <td><button type="submit" name="submit" formmethod="post" onclick="aceptarNegociacion(event, '¿Estas seguro que desea enviar la contraoferta')"class="btn btn-info">Enviar contraoferta</button></td>
                    </form>
                </tr>
            </table>
        </div>
    </div>  
    <form action="<?=site_url('HomeController/index'); ?>">
        <input type="submit" value="Regresar" class="btn btn-secondary"> 
    </form>
</body>
</html>