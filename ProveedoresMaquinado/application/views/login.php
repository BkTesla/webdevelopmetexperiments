<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<title>Login proveedores</title>
	</head>
	<body class="text-center">
		<div class="container text-center">
			<div class="row">
				<div class="col-sm-12">
                    <?php $image = array(
                        'src' => 'img/logo.png',
                        'class' => 'img-fluid',
                        'id' => 'logo'
                    );?>
                    <?php echo img($image);?>
				</div>
			</div>
        </div>
		<hr style="width: 100%; color: black; height: 1px; background-color:black;">
		<h1 class="h1">Bienvenido</h1>
		<hr style="width: 100%; color: black; height: 1px; background-color:black;">
		<!-- <p class="text-danger"> Complete todos los datos para poder ingresar</p> -->
		<br>
		<?php echo validation_errors(); ?>
		<?php echo form_open('LoginController/checkLogin'); ?>
		<input type="text" name="usuario" placeholder="Email" required/><br><br>
		<input type="password" name="password" placeholder="password"required/>
		<br>
		<br>
		<input type="submit" class="btn btn-primary" value="ingresar" name="submit"/>
		</form>
	</body>
</html>
