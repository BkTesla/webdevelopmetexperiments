<?php
  class HomeController extends CI_Controller
  {
    public function index()
    {
      $this->load->helper('html');
      $this->load->library('session');
      $listaCotizaciones['cotizaciones'] = array();
      $this->load->view('Home', $listaCotizaciones);
    }
                
    public function cargarDatos()
    {
      $estatus = $this->input->post('estatus');
      //var_dump($estatus);
      $this->load->helper('html');
      $this->load->model('CotizacionesModel');
      $usuario = $datos = $this->session->userdata('correo');
      $datosUsuario = $this->CotizacionesModel->IdProveedor($usuario);
     
      //$listaCotizaciones["cotizaciones"]= $this->CotizacionesModel->CargarProceso($datosUsuario, $estatus);
      $listaCotizaciones = $this->CotizacionesModel->CargarProceso($datosUsuario, $estatus);
      $datos = array(
        'cotizaciones' => $listaCotizaciones,
        'estatus' => $estatus
      );
      $this->load->view('Home', $datos);
    }
  }
 ?>
