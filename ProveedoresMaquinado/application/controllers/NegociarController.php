<?php
    Class NegociarController extends CI_Controller
    {
        public function index()
        {
            $this->load->helper('html');
            $datos = $this->input->post('negociar');
            $rutaPlano = $this->RutaPlano($datos);
            $datosNegociacion = $this->ConsultarNegociacion($datos);
            $imagen = $this->ConexionFtp($rutaPlano);

            $datos = array(
                'cargarImagen' => $imagen,
                'Datos' => $datosNegociacion
            );
            
            $this->load->view('Negociar',$datos);
        }

        public function ConexionFtp($rutaArchivo)
        {
            //$rutaArchivo = $rutaArchivo['Cargarimagen'];
            // $ftp_server = '192.168.0.250';
            // $ftp_user_name = 'codeigniter';
            // $ftp_user_pass = "codeigniter";
            // $local_file = '/opt/lampp/htdocs/login/img/imagen.jpg';
            // $server_file = $rutaArchivo;

            $ftp_server = '187.189.78.43';
            $ftp_user_name = 'vhuggo.campos@gmail.com';
            $ftp_user_pass = "8bfd9a089654b93714142bf35b4ff6ba";
            $local_file = '/opt/lampp/htdocs/login/img/imagen.jpg';
            $server_file = $rutaArchivo;

            $conn_id = ftp_connect($ftp_server);       
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

            $nlist = ftp_nlist($conn_id, $server_file);

            if(!empty($nlist)){
                ftp_get($conn_id, $local_file, $server_file, FTP_BINARY);
            }
            ftp_close($conn_id);

            return $nlist;
        }

        public function RutaPlano($DatosCotizacion)
        {
            $split = explode(',', $DatosCotizacion);
            $idCotizacion = $split[0];
            $this->load->helper('cookie');
            $cookie = array(
                'name' => 'id',
                'value' => $idCotizacion,
                'expire' => '86500'
            );

            set_cookie($cookie);
            $InformacionPlano = $split[1];
            $idPlano = $split[2];
            $nombrePlano = explode('-', $InformacionPlano);
            $subEnsamble = $nombrePlano[1];
            $nombrePlano = $nombrePlano[0];
            $rutaRaiz = ltrim($nombrePlano, 'M');
            $rutaArchivo = '/'.$rutaRaiz.'/'.$nombrePlano.'/'.$subEnsamble.'/'.'CP/'.$idPlano.' '.$InformacionPlano.'.PNG';
            return $rutaArchivo;
        }

        public function ConsultarNegociacion($datosProceso)
        {
            $splitDatos = explode(',', $datosProceso);
            $id = $splitDatos[0];

            $this->load->model('NegociarModel');
            $query = $this->NegociarModel->CargarNegociacion($id);
            return $query;
        }

        public function AceptarNegociacion()
        {
            $this->load->helper('cookie');
            $id = get_cookie('id');
            $datosCotizacion = $this->input->post('aceptar');
            
            $split = explode(',', $datosCotizacion);
            $precioFinal = $split[0];
            $tiempoFinal = $split[1];
            
            $this->load->model('NegociarModel');
            $estatus = 'NEGOCIACION ACEPTADA';
            $this->NegociarModel->AceptarRechazarNegociacion($id, $precioFinal, $tiempoFinal, $estatus);

            $this->load->view('Home');

        }
        public function RechazarNegociacion()
        {
            $this->load->helper('cookie');
            $id = get_cookie('id');
            $datosCotizacion = $this->input->post('rechazar');
            $split = explode(',', $datosCotizacion);
            $precioFinal = $split[0];
            $tiempoFinal = $split[1];
            $this->load->model('NegociarModel');
            $estatus = 'NEGOCIACION RECHAZADA';
            $this->NegociarModel->AceptarRechazarNegociacion($id, $precioFinal, $tiempoFinal, $estatus);

            $this->load->view('Home');

        }

        public function Contraoferta()
        {
            $this->load->helper('cookie');
            $id = get_cookie('id');
            $precioFinal = $this->input->post('precio');
            $tiempoFinal = $this->input->post('tiempo');
            $this->load->model('NegociarModel');
            $estatus = 'CONTRAOFERTA';
            $this->NegociarModel->AceptarRechazarNegociacion($id, $precioFinal, $tiempoFinal, $estatus);

            $this->load->view('Home');
        }
    }
?>