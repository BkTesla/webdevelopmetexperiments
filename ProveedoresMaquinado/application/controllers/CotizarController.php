<?php    
    Class CotizarController extends CI_Controller{

        public function index()
        {   
            $this->load->helper('html');
            $plano = $this->input->post('cotizar');
            $DatosCotizacion['imagen'] =$this->RutaPlano($plano);
            $imagen= $this->ConexionFtp($DatosCotizacion);
            $imagen = $imagen[0];
            $opcion= $this->opcion($plano);

            $datos = array(
                'opciones' => $opcion,
                'cargarImagen' => $imagen
            );
            $this->load->view('Cotizar', $datos);
        }

        public function ConexionFtp($rutaArchivo)
        {
                $rutaArchivo = $rutaArchivo['imagen'];
                // $ftp_server = '192.168.0.250';
                // $ftp_user_name = 'codeigniter';
                // $ftp_user_pass = "codeigniter";
                // $local_file = '/opt/lampp/htdocs/login/img/imagen.jpg';

                $ftp_server = '187.189.78.43';
                $ftp_user_name = 'vhuggo.campos@gmail.com';
                $ftp_user_pass = "8bfd9a089654b93714142bf35b4ff6ba";
                $local_file = '/opt/lampp/htdocs/login/img/imagen.jpg';
                $server_file = $rutaArchivo;

                $conn_id = ftp_connect($ftp_server);       
                $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

                $nlist = ftp_nlist($conn_id, $server_file);

                if(!empty($nlist)){
                    //echo 'no se encontro el archivo';
                    ftp_get($conn_id, $local_file, $server_file, FTP_BINARY);
                }
                
                ftp_close($conn_id);

                return $nlist;
        }

        public function RutaPlano($DatosCotizacion)
        {
            $split = explode(',', $DatosCotizacion);
            $idCotizacion = $split[0];
            $this->load->helper('cookie');
            $cookie = array(
                'name' => 'id',
                'value' => $idCotizacion,
                'expire' => '86500'
            );

            set_cookie($cookie);
            $InformacionPlano = $split[1];
            $idPlano = $split[2];
            $nombrePlano = explode('-', $InformacionPlano);
            $subEnsamble = $nombrePlano[1];
            $nombrePlano = $nombrePlano[0];
            $rutaRaiz = ltrim($nombrePlano, 'M');
            $rutaArchivo = '/'.$rutaRaiz.'/'.$nombrePlano.'/'.$subEnsamble.'/'.'CP/'.$idPlano.' '.$InformacionPlano.'.PNG';
            return $rutaArchivo;
        }
        
        public function CotizarConMaterial()
        {
            $this->load->helper('cookie');
            $opcion = 1;
            $id = get_cookie('id');
            $this->load->model('CotizarModel');
            $precioInicial = $this->input->post('precio');
            $tiempoInicial = $this->input->post('tiempo');
            $this->CotizarModel->Cotizar($id, $precioInicial, $tiempoInicial, $opcion);
            $this->load->view('Home');
        }

        public function CotizarSinMaterial()
        {
            $this->load->helper('cookie');
            $id = get_cookie('id');
            $opcion = 2;
            
            $this->load->model('CotizarModel');
            $precioInicial = $this->input->post('precio');
            $tiempoInicial = $this->input->post('tiempo');
            $this->CotizarModel->Cotizar($id, $precioInicial, $tiempoInicial, $opcion);
            $this->load->view('Home');
        }

        public function ExtraWork()
        {
            $this->load->helper('cookie');
            $id = get_cookie('id');
            //var_dump($id);
            $opcion = 3;
   
            //var_dump($id);
            $this->load->model('CotizarModel');
            $precioInicial = $this->input->post('precio');
            $tiempoInicial = $this->input->post('tiempo');
            //var_dump($precioInicial);
            //var_dump($tiempoInicial);
            $this->CotizarModel->Cotizar($id, $precioInicial, $tiempoInicial, $opcion);
            $this->load->view('Home');
        }

        public function opcion($DatosCotizacion)
        {
            $Datos = explode(',', $DatosCotizacion);
            $opcion = $Datos[3];
            return $opcion;
        }   
    }
?>