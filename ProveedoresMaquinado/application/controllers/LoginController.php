<?php
  class LoginController extends CI_Controller
  {
    public function index()
    {
      $this->load->helper('html');
      $this->load->view('login');
    }

    public function checkLogin()
    {
      $this->form_validation->set_rules('usuario', 'Usuario', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required|callback_verifyUser');

      if($this->form_validation->run() == FALSE){
        $this->load->view('login');
      }
      else{
        redirect('HomeController/index/');
      }
    }

    public function verifyUser()
    {
      $name = $this->input->post('usuario');
      $pass = $this->input->post('password');
      $pass = md5($pass);

      $this->load->model('LoginModel');
      if($this->LoginModel->Login($name, $pass)){
        $datosUsuario = array('correo' => $name);
	      $this->session->set_userdata($datosUsuario);
        
        return true;
      }

      else{
        $this->load->helper('html');
        $this->form_validation->set_message('verifyUser', 'el usuario o la contrasena son incorrectos');
        return false;
      }
    }
  }
 ?>
