<?php
  class CotizacionesModel extends CI_Model
  {
    public function CargarProceso($idUsuario, $estatus)
    {

      $joinQuery = 'procesos_cotizaciones.proveedor =';
      $joinQuery .= (string) $idUsuario;
      $joinQuery .=  ' AND procesos_cotizaciones.estatus =';
      $joinQuery .= "\"".$estatus."\"";
      $joinQuery .= ' AND procesos_cotizaciones.proceso = planos_procesos.id';

      $this->db->select('procesos_cotizaciones.id,procesos_cotizaciones.opcion, procesos_cotizaciones.proveedor,procesos_cotizaciones.estatus, planos_procesos.proceso, planos_proyecto.id AS id_plano, planos_proyecto.nombre_archivo');
      $this->db->from('procesos_cotizaciones');
      $this->db->join('planos_procesos', $joinQuery, 'inner', FALSE);
      $this->db->join('planos_proyecto','planos_procesos.plano = planos_proyecto.id','inner', FALSE);
      $this->db->order_by('planos_proyecto.nombre_archivo', 'asc');

      $query = $this->db->get();
      //var_dump($query);
      return $query;
    }

    public function IdProveedor($correo){

      $sql = 'select id_proveedor from usuarios_cb WHERE correo=?';
      $query =$this->db->query($sql, $correo);

      //echo $query->row()->id_proveedor;
      $id = $query->row()->id_proveedor;
      return $id;
    }
  }
?>
