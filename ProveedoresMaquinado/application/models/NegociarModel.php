<?php
    class NegociarModel extends CI_Model
    {
        public function CargarNegociacion($idProcesoCotizacion)
        {
            $sql = 'select precio_inicial, tiempo_inicial, precio_negociacion, tiempo_negociacion from procesos_cotizaciones WHERE id=?';
            $query = $this->db->query($sql, $idProcesoCotizacion); 
            $precioInicial =  $query->row()->precio_inicial;
            $datosNegociacion = array(
                'precioInicial' => $query->row()->precio_inicial,
                'tiempoInicial' => $query->row()->tiempo_inicial,
                'precioNegociacion' => $query->row()->precio_negociacion,
                'tiempoNegociacion' => $query->row()->tiempo_negociacion
            );
            return $datosNegociacion;
        }

        public function AceptarRechazarNegociacion($idProcesoCotizacion, $precioFinal, $tiempoFinal, $estatus)
        {
            $datos = array(
                'precio_final' => $precioFinal,
                'tiempo_final' => $tiempoFinal,
                'estatus' => $estatus  
            );

            $this->db->where('id', $idProcesoCotizacion);
            $this->db->update('procesos_cotizaciones', $datos); 
        }
    }
?>