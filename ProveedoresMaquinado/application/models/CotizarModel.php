<?php
    Class CotizarModel extends CI_Model
    {
        public function Cotizar($idProcesoCotizacion,$precioInicial, $tiempoInicial, $opcion )
        {
            $datos = array(
                'precio_inicial' => $precioInicial,
                'tiempo_inicial' => $tiempoInicial,
                'opcion' => $opcion,
                'estatus' => 'COTIZADO'
            );

            $this->db->where('id', $idProcesoCotizacion);
            $this->db->update('procesos_cotizaciones', $datos); 
        }
    }
?>