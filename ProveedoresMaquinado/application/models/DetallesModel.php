<?php
    class DetallesModel extends CI_Model
    {
        public function CargarDetalles($idProceso)
        {
            $sql = 'select precio_inicial, tiempo_inicial, precio_final, tiempo_final, precio_negociacion, tiempo_negociacion,  estatus from procesos_cotizaciones WHERE id=?';
            $query = $this->db->query($sql, $idProceso); 

            $precioInicial =  $query->row()->precio_inicial;
            $datosNegociacion = array(
                'precioInicial' => $query->row()->precio_inicial,
                'tiempoInicial' => $query->row()->tiempo_inicial,
                'tiempoNegociacion' => $query->row()->tiempo_negociacion,
                'precioNegociacion'=> $query->row()->precio_negociacion,
                'precioFinal' => $query->row()->precio_final,
                'tiempoFinal' => $query->row()->tiempo_final,
                'estatus' => $query->row()->estatus
            );
            return $datosNegociacion;
        }
    }
?>